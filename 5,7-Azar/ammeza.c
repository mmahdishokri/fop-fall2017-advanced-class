#include <stdio.h>

int n;
int c[121];
int isInRes[121];

void f(int i, int k, int curSum)
{
	if(i == n)
	{
		if(k == 0)
			isInRes[curSum] = 1;
		return;
	}
	f(i + 1, k, curSum);
	if(k >= c[i])
	{
		f(i + 1, k - c[i], curSum);
		f(i + 1, k - c[i], curSum + c[i]);
	}
}

int main()
{
	int k;
	scanf("%d%d", &n, &k);
	int i;
	for(i = 0; i < n; i++)
		scanf("%d", c + i);
	f(0, k, 0);
	for(i = 0; i <= 121; i++)
		if(isInRes[i])
			printf("%d ", i);
	printf("\n");
}
