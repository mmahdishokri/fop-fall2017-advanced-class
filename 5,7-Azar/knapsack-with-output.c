#include <stdio.h>

int a[2002], v[2002], n;
int e[2002];
int bestValue, bestAns[2002];

int f(int i, int V)
{
	if(i == n)
	{
		int j;
		int curValue = 0;
		for(j = 0; j < n; j++)
			if(e[j] == 1)
				curValue += v[j];
		if(curValue > bestValue)
		{
			bestValue = curValue;
			for(j = 0; j < n; j++)
				bestAns[j] = e[j];
		}
		return 0;
	}

	e[i] = 0;
	int res = f(i + 1, V);
	
	if(a[i] <= V)
	{
		e[i] = 1;
		int res2 = f(i + 1, V - a[i]) + v[i];
		if(res2 >= res)
			res = res2;
	}
	return res;
}

int main()
{
	int V;
	scanf("%d%d", &V, &n);
	int i;
	for(i = 0; i < n; i++)
		scanf("%d%d", a + i, v + i);
	printf("%d\n", f(0, V));
	for(i = 0; i < n; i++)
		if(bestAns[i] == 1)
			printf("%d ", i);
	printf("\n");
}
