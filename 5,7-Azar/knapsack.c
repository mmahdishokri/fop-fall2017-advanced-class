#include <stdio.h>

int a[2002], v[2002], n;

int f(int i, int V)
{
	if(i == n)
		return 0;
	int res = f(i + 1, V);
	if(a[i] <= V)
	{
		int res2 = f(i + 1, V - a[i]) + v[i];
		if(res2 >= res)
			res = res2;
	}
	return res;
}

int main()
{
	int V;
	scanf("%d%d", &V, &n);
	int i;
	for(i = 0; i < n; i++)
		scanf("%d%d", a + i, v + i);
	printf("%d\n", f(0, V));
}
