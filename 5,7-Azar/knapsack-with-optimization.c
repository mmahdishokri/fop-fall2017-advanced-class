#include <stdio.h>

int a[2002], v[2002], n;
int cache[2002][2002];

int f(int i, int V)
{
	if(i == n)
		return 0;
	if(cache[i][V] != -1)
		return cache[i][V];
	int res = f(i + 1, V);
	if(a[i] <= V)
	{
		int res2= f(i + 1, V - a[i]) + v[i];
		if(res2 >= res)
			res = res2;
	}
	cache[i][V] = res;
	return res;
}

int main()
{
	int V;
	scanf("%d%d", &V, &n);
	int i, j;
	for(i = 0; i <= n; i++)
		for(j = 0; j <= V; j++)
			cache[i][j] = -1;
	for(i = 0; i < n; i++)
		scanf("%d%d", a + i, v + i);
	printf("%d\n", f(0, V));
}
